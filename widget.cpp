#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    enableMoney();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::enableMoney()
{
    ui->pushButton_5->setEnabled(money >= 100);
    ui->pushButton_6->setEnabled(money >= 150);
    ui->pushButton_7->setEnabled(money >= 200);
}

void Widget::on_pushButton_clicked()
{
    money += 10;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_2_clicked()
{
    money += 50;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_3_clicked()
{
    money += 100;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_7_clicked()
{
    money -= 200;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_4_clicked()
{
    money += 150;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_5_clicked()
{
    money -= 100;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_6_clicked()
{
    money -= 150;
    ui->lcdNumber->display(money);
    enableMoney();
}


void Widget::on_pushButton_8_clicked()
{
    int fiveh = 0;
    int oneh = 0;
    int fifty = 0;
    int ten = 0;

    QMessageBox MsgBox;

    while (money >= 500)
    {
        fiveh ++;
        money -= 500;
    }

    while (money >= 100)
    {
        oneh ++;
        money -= 100;
    }

    while (money >= 50)
    {
        fifty ++;
        money -= 50;
    }

    while (money >= 10)
    {
        ten ++;
        money -= 10;
    }

    QString MsgStr = QString("500원 : %1, 100원 : %2, 50원 : %3, 10원 : %4").arg(fiveh).arg(oneh).arg(fifty).arg(ten);
    MsgBox.setText(MsgStr);
    MsgBox.exec();

    money = 0;
    ui->lcdNumber->display(money);
    enableMoney();
}

